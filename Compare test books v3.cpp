#include <fstream>
#include <iostream>
#include <unordered_map>
#include <queue>
#include <vector>
#include <string>
#include <set>
#include <boost\filesystem\path.hpp>
#include <boost\filesystem\operations.hpp>
#include <boost\filesystem\fstream.hpp>

#include <boost\serialization\unordered_map.hpp>
#include <boost\serialization\vector.hpp> 
#include <boost\serialization\string.hpp>
#include <boost\archive\binary_oarchive.hpp> 
#include <boost\archive\binary_iarchive.hpp>

namespace ba = boost::archive;
namespace fs = boost::filesystem;
std::set<char> punctuation = { '.', ',', ';', ':', '/', '\\', '\'', '\"', '!', '?', '-', '+', '_', '=', '<', '>',
'|', '*', '&', '^', '%', '$', '#', '�', '@', '~', '(', ')' };
std::set<std::string> stopwords = { "�", "���", "����������", "���������", "���", "�����", "����", "�����", "������", "�������", "�������", "�����", "�����", "��", "������", "������", "���", "����", "����", "����", "������", "����", "�", "��������", "�����", "���", "���", "���", "����", "������", "������", "��������", "�����", "�����", "����", "������", "������", "������", "�����", "���������", "��������", "�����������", "����", "������", "������", "������", "���������", "�����", "������", "������", "�����", "����", "������", "���", "����", "�����", "������", "��", "��������", "��������", "��������", "������", "����", "��������", "����������", "������", "��������", "���", "�������", "�������", "�����", "�����", "���", "������", "�����", "����", "����", "�����", "����", "���", "��", "��", "�������", "����������", "����������", "���", "�������", "�������", "������", "����������", "��������", "���������", "��������", "������", "�������", "���", "�����", "��", "�����", "������", "�����", "����", "���", "�����", "������", "������", "����", "���", "�������������", "����", "������", "����", "����", "�����", "���", "��", "�������", "������", "��������", "����������", "�������", "������", "������", "���", "��", "��", "���", "����", "����", "���", "���", "�", "�", "�����", "����", "��", "�����", "�����", "��", "�����", "��������", "�����", "�����", "�����", "�����", "������", "������", "�����", "������", "����", "�", "����", "��", "����", "��������", "���", "��", "�����", "�����", "������", "���������", "����������", "������", "����", "��", "�", "������", "�������", "������", "��������", "���", "�����", "�����", "�����", "��", "�����", "����", "�����", "�������", "�����", "������", "�������", "������", "�������", "�������", "������", "�������", "�����", "������", "���", "����", "�����", "��", "����", "������", "����", "�����", "�����", "�����", "����", "����", "�����", "�����", "����", "����", "���", "������", "������", "�����", "����", "�����", "�����", "�����", "���", "���", "���", "����", "���", "��", "�����", "��", "�������", "��������", "���������", "��������", "���", "�������", "����", "����������", "�������", "����", "��������", "��������", "���������", "�������", "��������", "��������", "�������", "���", "���������", "���", "����", "�����", "��", "������", "����", "���", "���", "���������", "������", "����������", "�������", "���������", "����������", "���������", "���������", "���", "������", "��", "��", "������", "�������", "���", "���", "������", "��", "�������", "��", "�����", "�", "��", "�������", "������������", "�����", "�������", "�����������", "�����������", "������", "������", "���������", "����", "������", "�����", "�����", "���������", "�����������", "�����", "��", "���", "���", "���", "����", "�����������", "�����������", "�����", "��", "�������", "����������", "��������", "������������", "���", "������", "��������", "�����", "�������", "������", "������", "������", "������", "�����", "��", "��������", "����������", "��������", "�������", "��������", "��������", "��������", "��������", "���", "����", "��������", "�������", "����������", "����������", "���������", "���������", "�������", "���������", "����", "����������", "��������", "��������", "�������", "���������", "����������", "������", "��������", "��������", "�������", "��������", "��������", "�����", "�����", "������", "������", "������", "�����", "������", "������", "������", "�������", "����������", "�����", "�����������", "����������������", "���������", "�����������", "������", "���������", "���", "����������", "���������", "��������", "�������", "�������������", "��������", "�������", "���", "������", "������", "�������", "�������", "������", "�����", "���", "�����", "����������", "�������", "�", "���", "����", "�����", "�����", "�����", "��������", "���������", "�����", "�����", "������", "����", "����", "�����", "�����", "����", "����", "�������", "������", "��������", "������", "�������", "������", "������", "�������", "�������", "�������", "������", "�������������", "���������", "�������", "����", "������", "�������", "�����", "������", "�����", "������", "������", "������", "�������", "��������", "�������", "�������", "������", "������", "�����", "�����", "��", "�����", "����������", "����������", "������", "�����������", "�����������", "���������", "��������", "���������", "����������", "��������������", "������������", "�������", "�����", "�����", "������", "�������", "�����", "�������", "������", "����", "����", "��������", "����������", "��������", "���������", "���������", "�������", "��", "���", "�����", "�����", "�����", "�����", "�����", "���", "������", "������", "����", "����", "��", "����", "����", "���", "������", "��", "�����", "����", "����", "���", "������", "���", "���", "������", "���", "������", "���", "��", "�", "������", "�����������", "���������", "��", "�����", "���", "������", "��������", "������", "����", "����", "������", "����", "�����", "����", "�����", "���������", "����", "������", "���", "�������", "��������", "���", "�����", "���", "����", "�����", "�����", "����", "���", "���", "���", "���", "���", "����", "���", "�����", "����", "����", "����", "���", "�", "�����", "", " " };
// ��������� ��� ���������� ������� ����� ��������
const double a = 1, b = 3, c = 2;

// ���������� ��� �������� �������� ������ ���������
int okay, all;

// ��������� ��� k ��������� �������
const double q = 0.61, r = 0.5;

std::unordered_map<std::string, std::vector<std::vector<std::unordered_map<std::string, int>>>> full;

int full_size(std::unordered_map<std::string, int>& text) {
    // ������� ������ ����� ������� ��� ������� ������ n-����� ������
    int k = 0;
    for (auto it = text.begin(); it != text.end(); ++it)
        k += it->second;
    return k;
}

std::string last_word(const std::string& str) {
    int x = str.size() - 1;
    std::string rez = "";
    while (str[x] != ' ') {
        rez.push_back(str[x]);
        --x;
    }
    std::reverse(rez.begin(), rez.end());
    return rez;
}

long long int cos_uni(std::unordered_map<std::string, int>& text1, std::unordered_map<std::string, int>& text2) {
    long long int rez = 0;
    for (auto it = text1.begin(); it != text1.end(); ++it) {
        // ������� ����������� ��� - ������ ����� �� n-������� � ������������� n ������� ������
        auto f_it = text2.find(it->first);
        // ��������� ���� �� �� ������ ������ ����� n-������
        if (f_it != text2.end())
            rez += it->second * f_it->second;
        // ���� ����, �� ��������� ������������ �� �������� � ����������
    }
    return rez;
}

double cos(std::unordered_map<std::string, int>& text_uni, std::unordered_map<std::string, int>& text_n,
    std::unordered_map<std::string, int>& unigramm, std::unordered_map<std::string, int>& ngramm) {
    // ������ ��- � ���- �����
    double rez = 0;
    for (auto it = ngramm.begin(); it != ngramm.end(); ++it) {
        // ��������� ���������
        auto f_it = text_n.find(it->first);
        if (f_it != text_n.end()) {
            std::string lw = last_word(it->first);
            if (unigramm[lw] != 0 && text_uni[lw] != 0)
                rez += ((double)it->second * f_it->second) / (unigramm[lw] * text_uni[lw]);
        }
        // ���� ����� n-������ ���� � ����������� ������, �� �������� ������������ ������������
    }
    return rez;
}

std::string maxim(std::unordered_map<std::string, short int>& pos, std::unordered_map<std::string, std::vector<double>>& vect) {
    double max = -1;
    std::string max_author = "";
    for (auto it = vect.begin(); it != vect.end(); ++it) {
        if (pos[it->first] < it->second.size() && max < it->second[pos[it->first]]) {
            max = it->second[pos[it->first]];
            max_author = it->first;
        }
    }
    return max_author;
}

std::string make_final_decision(std::unordered_map<std::string, std::vector<double>>& vect, std::string max_author) {
    // k ��������� �������
    double max_val = vect[max_author][0], max = max_val;
    int k = 0;
    std::string max_auth = max_author;
    std::unordered_map<std::string, double> after_sum;
    std::unordered_map<std::string, short int> pos;
    for (auto it = vect.begin(); it != vect.end(); ++it) {
        after_sum.insert({ it->first, 0 });
        pos.insert({ it->first, 0 });
    }
    while (max_val * r < max) {
        // ���� ������� ������������ ���������� �� ������ �������� ������ ��� �� (1 - r) ������ ��������
        ++pos[max_auth];
        after_sum[max_auth] += pow(q, k) * max;
        ++k;
        max_auth = maxim(pos, vect);
        if (max_auth == "")
            break;
        max = vect[max_auth][pos[max_auth]];
    }
    max = -1;
    for (auto it = after_sum.begin(); it != after_sum.end(); ++it) {
        if (it->second > max) {
            max = it->second;
            max_auth = it->first;
        }
    }
    return max_auth;
}

std::string guess(std::vector<std::unordered_map<std::string, int>>& text) {
    std::unordered_map<std::string, std::vector<double>> rezults = {};
    std::string author;
    std::vector<std::unordered_map<std::string, int>> dict;
    double rez1, rez2, rez3;
    for (auto auth_it = full.begin(); auth_it != full.end(); ++auth_it) {
        author = auth_it->first;
        // ������������� ��� ������ ������ ������ � ������� �� � �������
        for (auto text_it = full[author].begin(); text_it != full[author].end(); ++text_it) {
            // ������� ��� ������ (������� � ��������� ������)
            dict = *text_it;
            rez1 = (double)cos_uni(text[0], dict[0]) / (full_size(text[0]) * full_size(dict[0]));
            rez2 = cos(text[0], text[1], dict[0], dict[1]);
            rez3 = cos(text[0], text[2], dict[0], dict[2]);
            double rez = a*rez1 + b*rez2 + c*rez3;
            rezults[author].push_back(rez);
            // ��������� � ���������� ������ �� ���������
        }
        // ��������� � ���������� ������
    }
    double max = -1;
    std::string max_author = "";
    for (auto it = rezults.begin(); it != rezults.end(); ++it) {
        std::sort(rezults[it->first].begin(), rezults[it->first].end());
        std::reverse(rezults[it->first].begin(), rezults[it->first].end());
        // ������ � ������������ �� ������� ������ ���������
        if (rezults[it->first][0] > max) {
            // � �������� ����� ������� �������� ����������
            max = rezults[it->first][0];
            max_author = it->first;
        }
    }
    // ����� k ���������
    author = make_final_decision(rezults, max_author);
    return author;
}

std::string make_a_word(std::string line) {
    std::string word = "";
    char ch;
    for (int x = 0; x < line.size(); ++x) {
        ch = tolower(line[x]);
        if (punctuation.find(ch) == punctuation.end()) {
            word += ch;
        }
    }
    return word;
}

void add_ngramm(std::unordered_map<std::string, int>& dict, std::queue<std::string> ngramm) {
    std::string token = ngramm.front();
    ngramm.pop();
    for (int x = 0; 0 < ngramm.size(); ++x) {
        token = token + ' ' + ngramm.front();
        ngramm.pop();
    }
    dict.insert_or_assign(token, dict[token] + 1);
}

void make_ngramm(std::unordered_map<std::string, int>& dict, std::queue<std::string>& ngramm, std::string word, int n) {
    ngramm.push(word);
    if (ngramm.size() == n) {
        add_ngramm(dict, ngramm);
        ngramm.pop();
    }
}

std::string last_name(fs::path& m_root) {
    std::string str = m_root.string(), rez = {};
    int x = str.size() - 1;
    while (str[x] != '\\' && str[x] != '/')
        --x;
    for (++x; x < str.size(); ++x) {
        if (str[x] == ' ')
            rez += '_';
        else
            rez += str[x];
    }
    return rez;
}

void create_text(fs::path & m_root, std::string author) {
    fs::ifstream infile(m_root);
    std::string s;
    std::queue<std::string> unigramm = {}, bigramm = {}, trigramm = {};
    std::unordered_map<std::string, int> dict1, dict2, dict3;
    while (infile >> s) {
        std::string word = make_a_word(s);
        if (stopwords.find(word) == stopwords.end()) {
            make_ngramm(dict1, unigramm, word, 1);
            make_ngramm(dict2, bigramm, word, 2);
            make_ngramm(dict3, trigramm, word, 3);
        }
    }
    // ����� ��������� ��������� �� �� ���������, ��� � � ��������� ������� ��������� ������
    std::vector<std::unordered_map<std::string, int>> text = { dict1, dict2, dict3 };
    // �������� ������ � ����� ��������� n-����� ��� ������� ������
    std::string programm_decision = guess(text);
    // �������� ������� ��� ����� ������
    if (programm_decision == author)
        ++okay;
    else
        std::cout << last_name(m_root) << ", wrong ans.: " << author << " called " << programm_decision << std::endl;
    ++all;
}

void find_all_texts(fs::path & m_root, int k, std::string author = "") {
    if (k == 1)
        author = last_name(m_root);
    // ����� �� ���������� ������ �� ��������� ��� �� ������� last_name
    if (exists(m_root)) {
        if (is_directory(m_root)) {
            for (fs::path cur_dir : fs::directory_iterator(m_root)) {
                if (k == 1) {
                    create_text(cur_dir, author);
                    // �� ������ ������ ���������� ���������� ��������� ����� �� ���������
                } else
                    find_all_texts(cur_dir, k + 1, author);
            }
        }
    }
}

int main() {
    setlocale(LC_ALL, "Russian");
    okay = 0;
    all = 0;
    fs::path start_path = "C:/Users/Anton/Documents/Visual Studio 2015/Projects/Project NLP/Books_v1.2/Books_test";
    // start_path - ��������� ���������� �������� �������
    {
        std::ifstream file("BASE.DAT", std::ios::binary);
        ba::binary_iarchive infile(file);
        infile >> full;
    }
    find_all_texts(start_path, 0);
    // ������� ��� ������ ������ ����������
    std::cout << "����� ���� ������ - " << all << " , �� ��� ����� ���� �������� �� - " << okay;
    return 0;
}
