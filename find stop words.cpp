#include <algorithm>
#include <fstream>
#include <iostream>
#include <unordered_map>
#include <set>
#include <string>
#include <vector>

double x_times = 10;
// �� ������� ��� ������ ���������� ������ ������� ����� ��������� �������
double part = 3;
// n / part - ����� �������, � ������� ����������� ������ �����, ����� ������� ��� ����-������

bool compare(std::pair<int, std::string>& a, std::pair<int, std::string>& b) {
    return a.first > b.first;
}

bool is_a_stop_word(std::vector < std::set < std::string>>& stop_word, std::string word) {
    int n = stop_word.size();
    int k = 0;
    for (int x = 0; x < n; ++x) {
        int size_before = stop_word[x].size();
        stop_word[x].erase(word);
        if (stop_word[x].size() != size_before)
            ++k;
    }
    // ���� ����� word ����������� ���� �� � n / part ������� ��� ������ �����,
    // �� ��� �������� ����-������
    return k >= n / part;
}

int main() {
    // ������ ��������� - ����� ����-����� ��� ������� ������ �������
    std::ifstream file("BASE.DAT");
    // ��������� ���� � ������������ ��������� �������
    std::string reader1, reader2, counter, author;
    int count = 0, column, full_column, size;
    std::vector<std::set<std::string>> stop_word = {};
    std::vector<std::pair<int, std::string>> dict = {};
    while (file) {
        author = "";
        full_column = 0;
        file >> author;
        if (author == "")
            break;
        stop_word.push_back({});
        // �������� ��������� ���-������
        file >> counter;
        size = stoi(counter);
        for (int x = 0; x < size; ++x) {
            file >> reader1 >> reader2;
            column = stoi(reader2);
            dict.push_back(std::make_pair(column, reader1));
            full_column += column;
            // ������ �� ����� ����������� ���� � ������
        }
        std::sort(dict.begin(), dict.end(), compare);
        // ��������� ������ �� ������� �������� �� ��������
        double mid = (double)full_column / size;
        // ��������� ������� ���������� ��� ��� ���� ���-������ (�����)
        int k = 0;
        // ������ ������� � ����� ��������� ��� �����, ������� ����������� ���� �� �
        // ��� ���� ���� ��� �������
        while (k < size && dict[k].first >= x_times * mid) {
            stop_word[count].insert(dict[k].second);
            ++k;
        }
        // ---------------------------------------------------------------------------------------
        file >> counter;
        size = stoi(counter);
        for (int x = 0; x < size; ++x) {
            file >> reader1 >> reader1 >> reader2;
        }
        // ������ ������� (�� ��������� � �� �����������) ��-������
        file >> counter;
        size = stoi(counter);
        for (int x = 0; x < size; ++x) {
            file >> reader1 >> reader1 >> reader1 >> reader2;
        }
        // � ���-������
        // ---------------------------------------------------------------------------------------
        ++count;
    }
    file.close();
    // ����� ����� ����� �� ����� ������ �����, ������ ��� ������������� ����� ������ ������
    // �� ����������� ������ (������� ����������� �� ���� ��� ��������� �������).
    // ������ ��������� �������� ����� ��������� ���� ���������, ������� ������ ��� �
    // ���������� ����-�����
    std::vector<std::string> rez_stop_words = {};
    for (int x = 0; x < stop_word.size(); ++x) {
        while (stop_word[x].size() != 0) {
            // ���� �� �� �������� ��� ����� �� ������
            std::string word = *stop_word[x].begin();
            if (is_a_stop_word(stop_word, word))
                rez_stop_words.push_back(word);
            // ���� ����-�����, �� ��������� � ����������
        }
    }
    std::ofstream rez_file("STOP_WORDS.DAT");
    // ���������� � �������� ���� ��� ����-�����, ������� ��������, � ���������� �������
    std::sort(rez_stop_words.begin(), rez_stop_words.end());
    for (auto it = rez_stop_words.begin(); it != rez_stop_words.end(); ++it) {
        rez_file << '\"' << *it << '\"' << ',' << ' ';
    }
    rez_file.close();
    std::cout << rez_stop_words.size();
    return 0;
}
