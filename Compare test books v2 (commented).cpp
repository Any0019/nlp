#include <fstream>
#include <iostream>
#include <unordered_map>
#include <queue>
#include <vector>
#include <string>
#include <boost\filesystem\path.hpp>
#include <boost\filesystem\operations.hpp>
#include <boost\filesystem\fstream.hpp>

namespace fs = boost::filesystem;

const int a = 1, b = 3, c = 2;
// ��������� ��� ���������� ������� ����� ��������
int okay, all;
// ���������� ��� �������� �������� ������ ���������

int full_size(std::unordered_map<std::string, int>& text) {
    // ������� ������ ����� ������� ��� ������� ������ n-����� ������
    int k = 0;
    for (auto it = text.begin(); it != text.end(); ++it)
        k += it->second;
    return k;
}

long long int cos(std::unordered_map<std::string, int>& text1, std::unordered_map<std::string, int>& text2) {
    long long int rez = 0;
    for (auto it = text1.begin(); it != text1.end(); ++it) {
        // ������� ����������� ��� - ������ ����� �� n-������� � ������������� n ������� ������
        auto f_it = text2.find(it->first);
        // ��������� ���� �� �� ������ ������ ����� n-������
        if (f_it != text2.end())
            rez += it->second * f_it->second;
        // ���� ����, �� ��������� ������������ �� �������� � ����������
    }
    return rez;
}

double compare(std::vector<std::unordered_map<std::string, int>>& text, std::vector<std::unordered_map<std::string, int>>& atext) {
    std::vector<double> x(3);
    for (int i = 0; i < 3; ++i)
        x[i] = cos(text[i], atext[i]) / (full_size(text[i]) * full_size(text[i]));
    // ��� ������ n-������ ����������� ����� ������������
    // ���� �������� �������� ���������� cos ��� ��-����� � ���-����� (������ ����������� �� ����� �������� �����������) -> ��������!!!
    double rez = a*x[0] + b*x[1] + c*x[2];
    // ���������� � ��������� ����� �������� ��������� � ������������� 
    return rez;
}

std::string guess(std::vector<std::unordered_map<std::string, int>>& text) {
    std::unordered_map<std::string, std::vector<double>> rezults = {};
    double max = -1;
    std::string author;
    std::ifstream infile("BASE.DAT", std::ios::binary);
    // ��������� ���� BASE.DAT, ������� �� �������� ��� ��������
    // (��� ������ ����������� � ������� � ���������� ��������� �������� �������)
    infile.seekg(0);
    // ����� �� ������ �����
    std::pair<std::string, std::vector<std::unordered_map<std::string, int>>> atext;
    infile.read(reinterpret_cast<char*>(&atext), sizeof(std::pair<std::string, std::vector<std::unordered_map<std::string, int>>>));
    // ��������� ���������� �� ������� ������ �� ��������� ������� � atext
    while (infile) {
        // ����� �� ����� �����
        double x = compare(text, atext.second);
        // ������� ��� ������
        rezults[atext.first].push_back(x);
        // ���������� �������� ���������� 
        infile.read(reinterpret_cast<char*>(&atext), sizeof(std::pair<std::string, std::vector<std::unordered_map<std::string, int>>>));
        // ��������� � ���������� ������ �� ���������
    }
    infile.close();
    // ��������� ����, ����� �������� �� ���� �������
    for (auto it = rezults.begin(); it != rezults.end(); ++it) {
        std::sort(rezults[it->first].begin(), rezults[it->first].end());
        // ������ � ������������ �� ������� ������ ���������
        if (rezults[it->first][0] > max) {
            // � �������� ����� ������� �������� ����������
            max = rezults[it->first][0];
            author = it->first;
        }
    }
    return author;
    // ���� ��� �� ������ �������� ����� ������� �������� ���������� � �������,
    // ��� ���, � ���� ������� ����� ���������, � ���� ��� �����
}

std::string make_a_word(std::string line) {
    std::string word = "";
    char ch;
    for (int x = 0; x < line.size(); ++x) {
        ch = tolower(line[x]);
        if (ch >= '�' && ch <= '�' || ch >= 'a' && ch <= 'z' || ch == '�') {
            word += ch;
        }
    }
    return word;
}

void add_ngramm(std::unordered_map<std::string, int>& dict, std::queue<std::string> ngramm) {
    std::string token = ngramm.front();
    ngramm.pop();
    for (int x = 0; 0 < ngramm.size(); ++x) {
        token = token + ' ' + ngramm.front();
        ngramm.pop();
    }
    dict.insert_or_assign(token, dict[token] + 1);
}

void make_ngramm(std::unordered_map<std::string, int>& dict, std::queue<std::string>& ngramm, std::string word, int n) {
    ngramm.push(word);
    if (ngramm.size() == n) {
        add_ngramm(dict, ngramm);
        ngramm.pop();
    }
}

std::string last_name(fs::path& m_root) {
    std::string str = m_root.string(), rez = {};
    int x = str.size() - 1;
    while (str[x] != '\\' && str[x] != '/')
        --x;
    for (++x; x < str.size(); ++x)
        rez += str[x];
    return rez;
}

void create_text(fs::path & m_root, std::string author) {
    fs::ifstream infile(m_root);
    std::string s;
    std::queue<std::string> unigramm = {}, bigramm = {}, trigramm = {};
    std::unordered_map<std::string, int> dict1, dict2, dict3;
    while (infile >> s) {
        std::string word = make_a_word(s);
        if (word.size() > 3) {
            make_ngramm(dict1, unigramm, word, 1);
            make_ngramm(dict2, bigramm, word, 2);
            make_ngramm(dict3, trigramm, word, 3);
        }
    }
    // ����� ��������� ��������� �� �� ���������, ��� � � ��������� ������� ��������� ������
    std::vector<std::unordered_map<std::string, int>> text = { dict1, dict2, dict3 };
    // �������� ������ � ����� ��������� n-����� ��� ������� ������
    std::string programm_decision = guess(text);
    // �������� ������� ��� ����� ������
    if (programm_decision == author)
        ++okay;
    else
        std::cout << last_name(m_root) << "  real author: " << author << ";  program said: " << programm_decision << std::endl;
    ++all;
}

void find_all_texts(fs::path & m_root, int k, std::string author = "") {
    if (k == 1)
        author = last_name(m_root);
    // ����� �� ���������� ������ �� ��������� ��� �� ������� last_name
    if (exists(m_root)) {
        if (is_directory(m_root)) {
            for (fs::path cur_dir : fs::directory_iterator(m_root)) {
                if (k == 1) {
                    create_text(cur_dir, author);
                    // �� ������ ������ ���������� ���������� ��������� ����� �� ���������
                } else
                    find_all_texts(cur_dir, k + 1, author);
            }
        }
    }
}

int main() {
    setlocale(LC_ALL, "Russian");
    okay = 0;
    all = 0;
    fs::path start_path = "C:/Users/Anton/Documents/Visual Studio 2015/Projects/Project NLP/Books_v1.2/Books_test";
    // start_path - ��������� ���������� �������� �������
    find_all_texts(start_path, 0);
    // ������� ��� ������ ������ ����������
    return 0;
}
