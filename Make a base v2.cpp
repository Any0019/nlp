﻿#include <fstream>
#include <iostream>
#include <unordered_map>
#include <queue>
#include <vector>
#include <string>
#include <set>
#include <boost\filesystem\path.hpp>
#include <boost\filesystem\operations.hpp>
#include <boost\filesystem\fstream.hpp>

namespace fs = boost::filesystem;
std::set<char> punctuation = { '.', ',', ';', ':', '/', '\\', '\'', '\"', '!', '?', '-', '+', '_', '=', '<', '>',
'|', '*', '&', '^', '%', '$', '#', '№', '@', '~', '(', ')', ' ' };
std::set<std::string> stopwords = { "ddnbspnbsp", "font", "а", "алекс", "андрей", "анри", "анцифер", "аэропорта", "банка", "без", "бодрэ", "более", "больше", "брайан", "будет", "будто", "бы", "был", "была", "были", "было", "быть", "в", "вайнти", "вам", "вас", "вдруг", "ведь", "во", "вообще", "вот", "впрочем", "время", "все", "всегда", "всего", "всех", "всю", "вы", "гвен", "где", "герреро", "горио", "да", "даже", "два", "действительно", "делать", "дело", "джанет", "джеймс", "джек", "дженсен", "джон", "димирест", "для", "до", "дойл", "дойла", "доктор", "другой", "его", "ее", "ей", "ему", "если", "есть", "еще", "ж", "же", "жена", "жизни", "жизнь", "за", "затем", "зачем", "здесь", "знаю", "и", "иван", "из", "или", "им", "иногда", "их", "к", "кажется", "как", "какая", "какой", "кейз", "керрик", "кивнул", "когда", "конечно", "король", "которые", "который", "коулмен", "кристина", "кто", "куда", "ли", "лишь", "лучше", "людей", "люди", "майами", "майлз", "малколм", "маргарет", "марго", "марша", "между", "меня", "милли", "милорд", "миссис", "мистер", "мне", "много", "моей", "может", "можно", "мой", "моя", "мы", "на", "над", "надо", "наконец", "нас", "натали", "наташа", "не", "него", "нее", "ней", "нельзя", "несколько", "нет", "ни", "нибудь", "никогда", "ним", "них", "ничего", "но", "ну", "нужно", "о", "об", "огилви", "один", "однако", "одоннел", "окиф", "он", "она", "они", "опять", "от", "ответил", "отеле", "отель", "отеля", "отмычка", "очень", "парень", "пассажиров", "патрик", "патрони", "перед", "пирсон", "питер", "питера", "по", "под", "пока", "полиции", "поль", "после", "потом", "потому", "почему", "почти", "премьерминистр", "при", "про", "просто", "раз", "разве", "ричардсон", "роско", "руби", "с", "сам", "самолет", "самолета", "своей", "свою", "себе", "себя", "сегодня", "сейчас", "синди", "синтия", "сказал", "сказала", "скиминок", "снова", "со", "совершенно", "совсем", "спенсер", "спросил", "сразу", "старый", "так", "такое", "такой", "там", "таня", "тебе", "тебя", "тем", "теперь", "то", "тогда", "того", "тоже", "только", "том", "тот", "трент", "три", "тут", "ты", "у", "уж", "уже", "уоррен", "уэйнрайт", "фармазон", "харви", "хауден", "хейворд", "хорошо", "хорхе", "хоть", "хотя", "хуанита", "хэррис", "чего", "человек", "человека", "чем", "через", "что", "чтоб", "чтобы", "чтото", "чуть", "эванхелиста", "эдвина", "эжен", "эйнсли", "элан", "эти", "этого", "этой", "этом", "этот", "эту", "я", " ", "" };


std::string make_a_word(std::string line) {
    std::string word = "";
    char ch;
    for (int x = 0; x < line.size(); ++x) {
        // Рассматриваем все символы слова
        ch = tolower(line[x]);
        // Все символы переводим в нижний регист
        if (punctuation.find(ch) == punctuation.end()) {
        // Если этот символ не является пунктуационным, то записываем его в итоговое слово
            word += ch;
        }
    }
    return word;
}

void add_ngramm(std::unordered_map<std::string, int>& dict, std::queue<std::string> ngramm) {
    std::string token = ngramm.front();
    ngramm.pop();
    for (int x = 0; 0 < ngramm.size(); ++x) {
        // Разбираем очередь по слову и получаем token - элемент n-граммы
        token = token + ' ' + ngramm.front();
        ngramm.pop();
    }
    dict.insert_or_assign(token, dict[token] + 1);
    // Если раньше в словаре его не было, то добавляем, иначе увеличиваем соответствующее ему значение на 1
}

void make_ngramm(std::unordered_map<std::string, int>& dict, std::queue<std::string>& ngramm, std::string word, int n) {
    ngramm.push(word);
    // Добавляем слово к текущей n-грамме
    if (ngramm.size() == n) {
        // Если слов в n-грамме ровно n, то записываем её в соответствующий словарь
        add_ngramm(dict, ngramm);
        ngramm.pop();
        // И удаляем то слово, которое было добавлено раньше остальных
    }
}

void write_map(std::ofstream& file, std::unordered_map<std::string, int>& dict) {
    // Записываем unordered_map как string-и и int-ы
    int size = dict.size();
    file << size << ' ';
    // file.write(reinterpret_cast<char*>(&size), sizeof(size));
    for (auto it = dict.begin(); it != dict.end(); ++it) {
        std::string ngramm = it->first;
        int column = it->second;
        file << ngramm << ' ' << column << ' ';
        // file.write(reinterpret_cast<char*>(&ngramm), sizeof(std::string));
        // file.write(reinterpret_cast<char*>(&column), sizeof(int));
    }
}

void add_to_base(std::ofstream& file, fs::path & m_root, std::string& author) {
    fs::ifstream infile(m_root);
    // Создаём поток для чтения данного файла
    std::string s;
    std::queue<std::string> unigramm = {}, bigramm = {}, trigramm = {};
    // В эти очереди будем добавлять слова, которые прошли проверку СТОП-слов и получать соответствующие n-граммы
    std::unordered_map<std::string, int> dict1, dict2, dict3;
    // Три словоря, в которые мы запишем все наши n-граммы
    while (infile >> s) {  // Берём одно слово из файла (от одного разделителя до следующего)
        std::string word = make_a_word(s);
        // Делаем из него слово в нужном виде
        if (stopwords.find(word) == stopwords.end()) {
            // Если длина слова больше 3 букв, то добавляем его к текущим спискам n-грамм
            // Тут можно вставить ещё и проверку на СТОП-слова!!!
            make_ngramm(dict1, unigramm, word, 1);
            make_ngramm(dict2, bigramm, word, 2);
            make_ngramm(dict3, trigramm, word, 3);
        }
    }
    // file.write(reinterpret_cast<char*>(&author), sizeof(author));
    file << author << ' ';
    write_map(file, dict1);
    write_map(file, dict2); 
    write_map(file, dict3);
    // Записываем в выходной DAT-файл информацию, полученную в ходе обработки
}

std::string last_name(fs::path& m_root) {
    // Метод для вытягивания имени и фамилии автора текста из закголовка директории (уровня погружения 1)
    std::string str = m_root.string(), rez = {};
    int x = str.size() - 1;
    while (str[x] != '\\' && str[x] != '/')
        --x;
    for (++x; x < str.size(); ++x) {
        if (str[x] == ' ')
            rez += '_';
        else
            rez += str[x];
    }
    return rez;
}

void create_base(std::ofstream& file, fs::path & m_root, int k, std::string author = "") {
    // Реализация обхода директории дял поиска текстов
    if (k == 1)
        author = last_name(m_root);
    // На первом уровне погружения запоминаем автора текстов внутри данной директории
    if (exists(m_root)) {
        if (is_directory(m_root)) {
            for (fs::path cur_dir : fs::directory_iterator(m_root)) {
                // Если текущая папка является директорией и находится на втором уровне погружения,
                // то по очереди добавь все тексты из неё к базе
                if (k == 1)
                    add_to_base(file, cur_dir, author);
                // Добавляем текстовые файлы к базе
                else
                    create_base(file, cur_dir, k + 1, author);
            }
        }
    }
}

int main() {
    setlocale(LC_ALL, "Russian");
    fs::path start_path = "C:/Users/Anton/Documents/Visual Studio 2015/Projects/Project NLP/Books_v1.2/Books_learn";
    // start_path - стартовая директория, в которой находятся книги для обучения
    std::ofstream outfile("BASE.DAT", std::ios::binary);
    // BASE.DAT - файл-результат обучения программы (в него записывается обработанная информация)
    create_base(outfile, start_path, 0);
    // Создаём базу на основе файлов в директории start_path,  записывая результат в BASE.DAT
    outfile.close();
    // В результате мы получаем файл BASE.DAT, который считаем результатом нашей обработки
    // в него по очереди были записаны все информации по текстам в формате
    // pair<string, vector<unordered_map<string, int>>> - пара элементов
    // .first - автор (string), .second - вектор из трёх словарей для уни-, би- и три-грамм соответственно
    return 0;
}
