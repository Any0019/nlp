#include <fstream>
#include <iostream>
#include <unordered_map>
#include <queue>
#include <vector>
#include <string>
#include <boost\filesystem\path.hpp>
#include <boost\filesystem\operations.hpp>
#include <boost\filesystem\fstream.hpp>

namespace fs = boost::filesystem;

std::string make_a_word(std::string line) {
    std::string word = "";
    char ch;
    for (int x = 0; x < line.size(); ++x) {
        // ������������� ��� ������� �����
        ch = tolower(line[x]);
        // ��� ������� ��������� � ������ ������
        if (ch >= '�' && ch <= '�' || ch >= 'a' && ch <= 'z' || ch == '�') {
        // ���� ���� ������ �������� ������, �� ���������� ��� � �������� �����
        // ��� ��������� ������ ���������� �������� �� ��, ��� ������ - ����� !!!
            word += ch;
        }
    }
    return word;
}

void add_ngramm(std::unordered_map<std::string, int>& dict, std::queue<std::string> ngramm) {
    std::string token = ngramm.front();
    ngramm.pop();
    for (int x = 0; 0 < ngramm.size(); ++x) {
        // ��������� ������� �� ����� � �������� token - ������� n-������
        token = token + ' ' + ngramm.front();
        ngramm.pop();
    }
    dict.insert_or_assign(token, dict[token] + 1);
    // ���� ������ � ������� ��� �� ����, �� ���������, ����� ����������� ��������������� ��� �������� �� 1
}

void make_ngramm(std::unordered_map<std::string, int>& dict, std::queue<std::string>& ngramm, std::string word, int n) {
    ngramm.push(word);
    // ��������� ����� � ������� n-������
    if (ngramm.size() == n) {
        // ���� ���� � n-������ ����� n, �� ���������� � � ��������������� �������
        add_ngramm(dict, ngramm);
        ngramm.pop();
        // � ������� �� �����, ������� ���� ��������� ������ ���������
    }
}

void add_to_base(std::ofstream& file, fs::path & m_root, std::string& author) {
    fs::ifstream infile(m_root);
    // ������ ����� ��� ������ ������� �����
    std::string s;
    std::queue<std::string> unigramm = {}, bigramm = {}, trigramm = {};
    // � ��� ������� ����� ��������� �����, ������� ������ �������� ����-���� � �������� ��������������� n-������
    std::unordered_map<std::string, int> dict1, dict2, dict3;
    // ��� �������, � ������� �� ������� ��� ���� n-������
    while (infile >> s) {  // ���� ���� ����� �� ����� (�� ������ ����������� �� ����������)
        std::string word = make_a_word(s);
        // ������ �� ���� ����� � ������ ����
        if (word.size() > 3) {
            // ���� ����� ����� ������ 3 ����, �� ��������� ��� � ������� ������� n-�����
            // ��� ����� �������� ��� � �������� �� ����-�����!!!
            make_ngramm(dict1, unigramm, word, 1);
            make_ngramm(dict2, bigramm, word, 2);
            make_ngramm(dict3, trigramm, word, 3);
        }
    }
    std::pair<std::string, std::vector<std::unordered_map<std::string, int>>> base;
    // ������ ������� ����, ������� �������� � ����: ������ ������ + ��� ������� � ����������� ������ �� n-�����
    base.first = author;
    base.second = { dict1, dict2, dict3 };
    // ��������� ��� �����������, ���������� � ���� ���������
    file.write(reinterpret_cast<char*>(&base), sizeof(base));
    // ���������� � �������� DAT-���� ���� ����� ������� ����
}

std::string last_name(fs::path& m_root) {
    // ����� ��� ����������� ����� � ������� ������ ������ �� ���������� ���������� (������ ���������� 1)
    std::string str = m_root.string(), rez = {};
    int x = str.size() - 1;
    while (str[x] != '\\' && str[x] != '/')
        --x;
    for (++x; x < str.size(); ++x)
        rez += str[x];
    return rez;
}

void create_base(std::ofstream& file, fs::path & m_root, int k, std::string author = "") {
    // ���������� ������ ���������� ��� ������ �������
    if (k == 1)
        author = last_name(m_root);
    // �� ������ ������ ���������� ���������� ������ ������� ������ ������ ����������
    if (exists(m_root)) {
        if (is_directory(m_root)) {
            for (fs::path cur_dir : fs::directory_iterator(m_root)) {
                // ���� ������� ����� �������� ����������� � ��������� �� ������ ������ ����������,
                // �� �� ������� ������ ��� ������ �� �� � ����
                if (k == 1)
                    add_to_base(file, cur_dir, author);
                // ��������� ��������� ����� � ����
                else
                    create_base(file, cur_dir, k + 1, author);
            }
        }
    }
}

int main() {
    setlocale(LC_ALL, "Russian");
    fs::path start_path = "C:/Users/Anton/Documents/Visual Studio 2015/Projects/Project NLP/Books_v1.2/Books_learn";
    // start_path - ��������� ����������, � ������� ��������� ����� ��� ��������
    std::ofstream outfile("BASE.DAT", std::ios::binary);
    // BASE.DAT - ����-��������� �������� ��������� (� ���� ������������ ������������ ����������)
    create_base(outfile, start_path, 0);
    // ������ ���� �� ������ ������ � ���������� start_path,  ��������� ��������� � BASE.DAT
    outfile.close();
    // � ���������� �� �������� ���� BASE.DAT, ������� ������� ����������� ����� ���������
    // � ���� �� ������� ���� �������� ��� ���������� �� ������� � �������
    // pair<string, vector<unordered_map<string, int>>> - ���� ���������
    // .first - ����� (string), .second - ������ �� ��� �������� ��� ���-, ��- � ���-����� ��������������
    return 0;
}
