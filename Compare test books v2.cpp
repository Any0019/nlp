#include <fstream>
#include <iostream>
#include <unordered_map>
#include <queue>
#include <vector>
#include <string>
#include <set>
#include <boost\filesystem\path.hpp>
#include <boost\filesystem\operations.hpp>
#include <boost\filesystem\fstream.hpp>

namespace fs = boost::filesystem;
std::set<char> punctuation = { '.', ',', ';', ':', '/', '\\', '\'', '\"', '!', '?', '-', '+', '_', '=', '<', '>',
'|', '*', '&', '^', '%', '$', '#', '�', '@', '~', '(', ')'};
std::set<std::string> stopwords = { "ddnbspnbsp", "font", "�", "�����", "������", "����", "�������", "���������", "�����", "���", "�����", "�����", "������", "������", "�����", "�����", "��", "���", "����", "����", "����", "����", "�", "������", "���", "���", "�����", "����", "��", "������", "���", "�������", "�����", "���", "������", "�����", "����", "���", "��", "����", "���", "�������", "�����", "��", "����", "���", "�������������", "������", "����", "������", "������", "����", "�������", "����", "��������", "���", "��", "����", "�����", "������", "������", "���", "��", "��", "���", "����", "����", "���", "�", "��", "����", "�����", "�����", "��", "�����", "�����", "�����", "����", "�", "����", "��", "���", "��", "������", "��", "�", "�������", "���", "�����", "�����", "����", "������", "������", "�����", "�������", "������", "�������", "�������", "�������", "��������", "���", "����", "��", "����", "�����", "�����", "����", "������", "�����", "�������", "��������", "�����", "�����", "�����", "����", "�����", "������", "������", "������", "���", "�����", "����", "�����", "�����", "���", "���", "��", "��", "���", "����", "�������", "���", "������", "������", "��", "����", "���", "���", "������", "���������", "���", "��", "������", "�������", "���", "���", "������", "��", "��", "�����", "�", "��", "������", "����", "������", "�������", "����", "��", "���", "���", "�����", "��", "�������", "�����", "�����", "�����", "�������", "�����", "������", "����������", "������", "�������", "�����", "������", "�����", "������", "��", "���", "����", "�������", "����", "�����", "�����", "������", "������", "�����", "��������������", "���", "���", "������", "���", "�����", "���������", "�����", "����", "�", "���", "�������", "��������", "�����", "����", "����", "����", "�������", "������", "�����", "������", "������", "�������", "��������", "�����", "��", "����������", "������", "�������", "�������", "�����", "������", "���", "�����", "�����", "���", "����", "����", "����", "���", "������", "��", "�����", "����", "����", "������", "���", "���", "�����", "���", "���", "��", "�", "��", "���", "������", "��������", "��������", "�����", "������", "�������", "������", "�����", "����", "����", "�������", "������", "����", "�������", "��������", "���", "�����", "���", "����", "�����", "�����", "����", "�����������", "������", "����", "������", "����", "���", "�����", "����", "����", "����", "���", "�", " ", "" };
const int a = 1, b = 3, c = 2;
// ��������� ��� ���������� ������� ����� ��������
int okay, all;
// ���������� ��� �������� �������� ������ ���������

int full_size(std::unordered_map<std::string, int>& text) {
    // ������� ������ ����� ������� ��� ������� ������ n-����� ������
    int k = 0;
    for (auto it = text.begin(); it != text.end(); ++it)
        k += it->second;
    return k;
}

std::string last_word(const std::string& str) {
    int x = str.size() - 1;
    std::string rez = "";
    while (str[x] != ' ') {
        rez.push_back(str[x]);
        --x;
    }
    std::reverse(rez.begin(), rez.end());
    return rez;
}

long long int cos_uni(std::unordered_map<std::string, int>& text1, std::unordered_map<std::string, int>& text2) {
    long long int rez = 0;
    for (auto it = text1.begin(); it != text1.end(); ++it) {
        // ������� ����������� ��� - ������ ����� �� n-������� � ������������� n ������� ������
        auto f_it = text2.find(it->first);
        // ��������� ���� �� �� ������ ������ ����� n-������
        if (f_it != text2.end())
            rez += it->second * f_it->second;
        // ���� ����, �� ��������� ������������ �� �������� � ����������
    }
    return rez;
}

double compare(std::vector<std::unordered_map<std::string, int>>& text, std::unordered_map<std::string, int>& dict1,
    double rez2, double rez3) {
    double rez1;
    rez1 = (double) cos_uni(text[0], dict1) / (full_size(text[0]) * full_size(dict1));
    // ��� ������ n-������ ����������� ����� ������������
    double rez = a*rez1 + b*rez2 + c*rez3;
    // ���������� � ��������� ����� �������� ��������� � ������������� 
    return rez;
}

void read_map(std::ifstream& file, std::unordered_map<std::string, int>& dict) {
    // ������ �� ����� �������� � ����� ��������� � ���� �����
    std::string size_str;
    int size;
    file >> size_str;
    size = stoi(size_str);
    dict.clear();
    std::string ngramm, column_str;
    int column;
    for (int x = 0; file && x < size; ++x) {
        file >> ngramm;
        file >> column_str;
        column = stoi(column_str);
        dict.insert({ ngramm, column });
    }
}

double read_map(std::ifstream& file, int n, std::unordered_map <std::string, int>& uni_gramm, std::unordered_map<std::string, int>& text1) {
    // ������ � �������� ��- � ���- �����
    double rez = 0;
    std::string size_str;
    int size;
    file >> size_str;
    size = stoi(size_str);
    std::string ngramm, ngramm1, ngramm2, ngramm3, column_str;
    int column;
    for (int x = 0; file && x < size; ++x) {
        file >> ngramm1 >> ngramm2;
        ngramm = ngramm1 + ' ' + ngramm2;
        if (n == 3) {
            file >> ngramm3;
            ngramm += ' ' + ngramm3;
        }
        file >> column_str;
        column = stoi(column_str);
        // ��������� ���������
        auto f_it = text1.find(ngramm);
        if (f_it != text1.end())
            rez += ((double)column * f_it->second) / (uni_gramm[last_word(ngramm)] * uni_gramm[last_word(f_it->first)]);
        // ���� ����� n-������ ���� � ����������� ������, �� �������� ������������ ������������
    }
    return rez;
}

std::string guess(std::vector<std::unordered_map<std::string, int>>& text) {
    std::unordered_map<std::string, std::vector<double>> rezults = {};
    double max = -1;
    std::string author;
    std::ifstream infile("BASE.DAT", std::ios::binary);
    // ��������� ���� BASE.DAT, ������� �� �������� ��� ��������
    // (��� ������ ����������� � ������� � ���������� ��������� �������� �������)
    infile.seekg(0);
    // ����� �� ������ �����
    std::unordered_map<std::string, int> dict1;
    infile >> author;
    read_map(infile, dict1);
    double rez2, rez3;
    rez2 = read_map(infile, 2, dict1, text[1]);
    rez3 = read_map(infile, 3, dict1, text[2]);
    // ��������� ���������� �� ������� ������ �� ��������� ������� � atext
    while (infile) {
        // ����� �� ����� �����
        double x = compare(text, dict1, rez2, rez3);
        // ������� ��� ������
        rezults[author].push_back(x);
        // ���������� �������� ���������� 
        author = "";
        infile >> author;
        if (author == "" )
            break;
        read_map(infile, dict1);
        rez2 = read_map(infile, 2, dict1, text[1]);
        rez3 = read_map(infile, 3, dict1, text[2]);
        // ��������� � ���������� ������ �� ���������
    }
    infile.close();
    // ��������� ����, ����� �������� �� ���� �������
    for (auto it = rezults.begin(); it != rezults.end(); ++it) {
        std::sort(rezults[it->first].begin(), rezults[it->first].end());
        std::reverse(rezults[it->first].begin(), rezults[it->first].end());
        // ������ � ������������ �� ������� ������ ���������
        if (rezults[it->first][0] > max) {
            // � �������� ����� ������� �������� ����������
            max = rezults[it->first][0];
            author = it->first;
        }
    }
    return author;
    // ���� ��� �� ������ �������� ����� ������� �������� ���������� � �������,
    // ��� ���, � ���� ������� ����� ���������, � ���� ��� �����
}

std::string make_a_word(std::string line) {
    std::string word = "";
    char ch;
    for (int x = 0; x < line.size(); ++x) {
        ch = tolower(line[x]);
        if (punctuation.find(ch) == punctuation.end()) {
            word += ch;
        }
    }
    return word;
}

void add_ngramm(std::unordered_map<std::string, int>& dict, std::queue<std::string> ngramm) {
    std::string token = ngramm.front();
    ngramm.pop();
    for (int x = 0; 0 < ngramm.size(); ++x) {
        token = token + ' ' + ngramm.front();
        ngramm.pop();
    }
    dict.insert_or_assign(token, dict[token] + 1);
}

void make_ngramm(std::unordered_map<std::string, int>& dict, std::queue<std::string>& ngramm, std::string word, int n) {
    ngramm.push(word);
    if (ngramm.size() == n) {
        add_ngramm(dict, ngramm);
        ngramm.pop();
    }
}

std::string last_name(fs::path& m_root) {
    std::string str = m_root.string(), rez = {};
    int x = str.size() - 1;
    while (str[x] != '\\' && str[x] != '/')
        --x;
    for (++x; x < str.size(); ++x) { 
        if (str[x] == ' ')
            rez += '_';
        else
            rez += str[x];
    }
    return rez;
}

void create_text(fs::path & m_root, std::string author) {
    fs::ifstream infile(m_root);
    std::string s;
    std::queue<std::string> unigramm = {}, bigramm = {}, trigramm = {};
    std::unordered_map<std::string, int> dict1, dict2, dict3;
    while (infile >> s) {
        std::string word = make_a_word(s);
        if (stopwords.find(word) == stopwords.end()) {
            make_ngramm(dict1, unigramm, word, 1);
            make_ngramm(dict2, bigramm, word, 2);
            make_ngramm(dict3, trigramm, word, 3);
        }
    }
    // ����� ��������� ��������� �� �� ���������, ��� � � ��������� ������� ��������� ������
    std::vector<std::unordered_map<std::string, int>> text = { dict1, dict2, dict3 };
    // �������� ������ � ����� ��������� n-����� ��� ������� ������
    std::string programm_decision = guess(text);
    // �������� ������� ��� ����� ������
    if (programm_decision == author)
        ++okay;
    else
        std::cout << last_name(m_root) << "  real author: " << author << ";  program said: " << programm_decision << std::endl;
    ++all;
}

void find_all_texts(fs::path & m_root, int k, std::string author = "") {
    if (k == 1)
        author = last_name(m_root);
    // ����� �� ���������� ������ �� ��������� ��� �� ������� last_name
    if (exists(m_root)) {
        if (is_directory(m_root)) {
            for (fs::path cur_dir : fs::directory_iterator(m_root)) {
                if (k == 1) {
                    create_text(cur_dir, author);
                    // �� ������ ������ ���������� ���������� ��������� ����� �� ���������
                } else
                    find_all_texts(cur_dir, k + 1, author);
            }
        }
    }
}

int main() {
    setlocale(LC_ALL, "Russian");
    okay = 0;
    all = 0;
    fs::path start_path = "C:/Users/Anton/Documents/Visual Studio 2015/Projects/Project NLP/Books_v1.2/Books_test";
    // start_path - ��������� ���������� �������� �������
    find_all_texts(start_path, 0);
    // ������� ��� ������ ������ ����������
    std::cout << "����� ���� ������ - " << all << " , �� ��� ����� ���� �������� �� - " << okay;
    return 0;
}
